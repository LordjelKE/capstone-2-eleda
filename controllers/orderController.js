const Order = require('../models/orderModel');
const auth = require('../auth');

/*NON-ADMIN CONTROLLERS*/

//CREATE AN ORDER
module.exports.createOrder = (req, res) =>{
	let totalAmount = req.body.totalAmount;
	let userId = req.user.id;
	let userAndOrder = req.body.userAndOrder;

	let newOrder = new Order({
	  totalAmount: totalAmount,
	  userId: userId,
	  userAndOrder: userAndOrder
	});

		newOrder.save().then(order => {
			res.send(order)
		}).catch(err=> {
			res.send(err)
		})
}

//PLACE AN ORDER FOR A PRODUCT CONTROLLER
module.exports.orderProduct = (req, res) => {
	let userId = req.user.id;

	Order.findOne({userId: userId}).then(nonAdminUser => {
		let userAndOrder = nonAdminUser.userAndOrder

		let filterUserAndOrder = userAndOrder.filter((orderedProducts)=> {
			if(orderedProducts.productId == req.body.productId){
				return orderedProducts;
			}
		})

		if(filterUserAndOrder.length > 0){
			res.send({
				message: "Product has already been ordered!"
			})
		} else {
			nonAdminUser.userAndOrder.push(req.body)
			nonAdminUser.save()
			res.send(nonAdminUser)
		}

	}).catch(err=>{
		res.send({err})
	})
}

//FIND ORDERED PRODUCTS
module.exports.findorderList = (req, res) => {
	let userId = req.user.id
	Order.findOne({userId: userId}).then(nonAdminUser => {
		res.send({message: "Here are your orders!",orderList: nonAdminUser.userAndOrder})
	}).catch(err => {
		res.send(err);
	})
}


/*ADMIN CONTROLLERS*/

//FIND ALL ORDERS
module.exports.findAllOrders = (req, res) => {
	Order.find({}).then(foundAllOrders => {
		res.send({Orders: foundAllOrders})
	}).catch(err => {
		res.send(err);
	})
}