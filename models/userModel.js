const mongoose = require('mongoose');

//USER SCHEMA
const userSchema = new mongoose.Schema({
	firstName: {
		type: String, 
		required: [true, 'First name is required']
	}, 
	lastName: {
		type: String, 
		required: [true, 'Last name is required']
	},
	email: {
		type: String, 
		required: [true, 'Email is required']
	},
	password: {
		type: String, 
		required: [true, 'Password is required']
	},
	isAdmin: {
		type: Boolean, 
		default: false
	},
	mobileNo: {
		type: String, 
		required: [true, 'Mobile number is required']
	},
	shippingAddress: {
		type: String, 
		required: [true, 'Shipping Address is required']
	}
})

//USER MODEL 
module.exports = mongoose.model('User', userSchema);