const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

//REGISTER(POST)
router.post('/register', userController.register);

//LOGIN(POST)
router.post('/login', userController.login);


/*NON-ADMIN USER ROUTES*/

	//DETAILS(GET)
	router.get('/profile', auth.verify, userController.getUserDetails);

	//UPDATE DETAILS(PUT)
	router.put('/profile/update', auth.verify, auth.verifyIsOrdinaryUser, userController.updateUser);


/*ADMIN USER ROUTES*/

	//DETAILS(GET)
	router.get('/admin/profile', auth.verify, auth.verifyIsAdmin, userController.adminProfile);

	//SET USER AS ADMIN(PUT)
	router.put('/profile/update/admin', auth.verify, auth.verifyIsAdmin, userController.setUserToAdmin);

module.exports = router