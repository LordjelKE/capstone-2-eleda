const express = require('express');
const mongoose = require('mongoose');
const users = require('./routes/userRoute');
const products = require('./routes/productRoute');
const orders = require('./routes/orderRoute')
const cors = require('cors');
require('dotenv').config();

const app = express();
const port = process.env.PORT;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//DB CONNECTION
mongoose.connect(process.env.DB_MONGODB_ATLAS, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection error"));
db.on('open', ()=> console.log(`Connected to database successfully!`));


app.get('/', (req, res)=>{
	res.send("Welcome to My Shop!")
})

//MIDDLEWARE
app.use('/api', users)
app.use('/api', products)
app.use('/api', orders)

//SERVER LISTENING
app.listen(port, ()=> console.log(`Server is listening to port ${port}`));