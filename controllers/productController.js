const Product = require('../models/productModel');
const auth = require('../auth');

/*NON-ADMIN CONTROLLERS*/

//FIND ALL PRODUCTS
module.exports.findAllProducts = (req, res) => {
	Product.find({}).then(foundAllProducts => {
			res.send({IsActiveProducts: foundAllProducts})
	}).catch(err => {
		res.send(err);
	})
}

//FIND A PRODUCT
module.exports.findAProduct = (req, res) => {
	let productId = req.body.productId
	Product.find({_id: productId}).then(foundProduct => {
		res.send({Product: foundProduct})
	}).catch(err => {
		res.send(err);
	})
}


/*ADMIN CONTROLLERS*/

//CREATE A PRODUCT
module.exports.addProduct = (req, res) =>{

		let productName = req.body.productName;
		let description = req.body.description;
		let price = req.body.price;

		if(
			(productName != '' && description != '' && price != '' ) &&
			(productName != null && description != null && price != null ) &&
			(productName != undefined && description != undefined && price != undefined )
		){

			let newProduct = new Product({
				productName: productName,
				description: description,
				price: price
			})

			newProduct.save().then(product => {
				res.send(product)
			}).catch(err=> {
				res.send(err)
			})
		} else{
			res.send({
				message: "All fields are required!"
			})
		}
	}
	

//UPDATE A PRODUCT
module.exports.updateProduct = (req, res) => {
	let productId = req.body.productId

	let updates = {
		productName: req.body.productName,
		description: req.body.description,
		price: req.body.price
	}
	let options = {
		new: true
	}
	Product.findByIdAndUpdate(productId, updates, options).then(updatedProduct => {
		res.send({newUpdate: updatedProduct});
	}).catch(err=> {
		res.send(err);
	})
}

//ARCHIVE A PRODUCT 
module.exports.archiveProduct = (req, res) => {
	let productId = req.params.productId
  Product.findOne({_id: productId}).then(result => {
    if(result.isActive){
	      result.isActive = false;
	    	 result.save().then(archivedProduct => {
	    		res.send({
	    			message: "Product has been archived!",
					archivedData: archivedProduct
	    		})
	    	}).catch( err => {
	    		res.send(err);
	    	});
	      
	    } else {
	    	res.send({ message: "Product is already archived!" })
	    }
	  }).catch(err => {
	    res.send(err);
	  });
}

//ACTIVATE A PRODUCT
module.exports.activateProduct = (req, res) => {
		let productId = req.params.productId
	  Product.findOne({_id: productId}).then(result => {
	    if(result.isActive === false){
		      result.isActive = true;
		    	 result.save().then(activatedProduct => {
		    		res.send({
		    			message: "Product has been activated!",
						activatedData: activatedProduct
		    		})
		    	}).catch( err => {
		    		res.send(err);
		    	});
		      
		    } else {
		    	res.send({ message: "Product is already activated!" })
		    }
		  }).catch(err => {
		    res.send(err);
		  });
	}
