const mongoose = require('mongoose');

//ORDER SCHEMA
const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number, 
	}, 
	purchasedOn: {
		type: Date, 
		default: new Date()
	},
	userId: {
		type: String,
		required: [true, 'User ID is required']
	},
	userAndOrder: [
	{
		productId: {
			type: String,
			required: [true, 'Product ID is required']
		},
		orderedOn: {
			type: Date, 
			default: new Date()
		},
		status: {
				type: String, 
				default: 'Order has been placed'
			}
	}
	]
})

//ORDER MODEL 
module.exports = mongoose.model('Order', orderSchema);