const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');

/*NON-ADMIN ORDER ROUTES*/

	//CREATE AN ORDER (POST)
	router.post('/order/create', auth.verify, auth.verifyIsOrdinaryUser, orderController.createOrder)

	//ADD PRODUCT TO ORDER (POST)
	router.post('/order/add', auth.verify, auth.verifyIsOrdinaryUser, orderController.orderProduct);

	//FIND ORDER (GET)
	router.get('/order/products', auth.verify, auth.verifyIsOrdinaryUser, orderController.findorderList);

/*ADMIN ORDER ROUTES*/
	
	//FIND ALL ORDERS(GET)
	router.get('/order/find/admin', auth.verify, auth.verifyIsAdmin, orderController.findAllOrders);

module.exports = router