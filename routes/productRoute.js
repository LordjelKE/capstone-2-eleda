const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

/*NON-ADMIN PRODUCT ROUTES*/

	//FIND ALL PRODUCTS(GET)
	router.get('/products/findAll', productController.findAllProducts);

	//FIND A PRODUCT(GET)
	router.get('/products/find', auth.verify, productController.findAProduct);


/*ADMIN PRODUCT ROUTES*/

	//CREATE A PRODUCT(POST)
	router.post('/product/add/admin', auth.verify, auth.verifyIsAdmin, productController.addProduct);

	//UPDATE A PRODUCT(PUT)
	router.put('/product/update/admin/:productId', auth.verify, auth.verifyIsAdmin, productController.updateProduct);

	//ARCHIVE A PRODUCT (PUT)
	router.put('/product/archive/admin/:productId', auth.verify, auth.verifyIsAdmin, productController.archiveProduct);

	//ACTIVATE A PRODUCT (PUT)
	router.put('/product/activate/admin/:productId', auth.verify, auth.verifyIsAdmin, productController.activateProduct);

module.exports = router