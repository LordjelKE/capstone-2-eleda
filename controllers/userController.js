const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//REGISTER CONTROLLER
module.exports.register = (req, res) => {

	User.findOne({email: req.body.email}).then(result => {
		if(result != null){
			res.send({
				message: "Email has already been registered!"
			})
		}else {
			let firstName = req.body.firstName;
			let lastName = req.body.lastName;
			let email = req.body.email;
			let password = req.body.password;
			let mobileNo = req.body.mobileNo;
			let shippingAddress = req.body.shippingAddress;

			if(
				(firstName != '' && lastName != '' && email != '' && password != '' && mobileNo != '' && shippingAddress != '') &&
				(firstName != null && lastName != null && email != null && password != null && mobileNo != null && shippingAddress != null) &&
				(firstName != undefined && lastName != undefined && email != undefined && password != undefined && mobileNo != undefined && shippingAddress != undefined)
			){
				const hashedPassword = bcrypt.hashSync(req.body.password, 10)

				let newUser = new User({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: hashedPassword,
					mobileNo: mobileNo,
					shippingAddress: shippingAddress
				})

				newUser.save().then(user => {
					res.send(user)
				}).catch(err=> {
					res.send(err)
				})
			} else{
				res.send({
					message: "All fields are required!"
				})
			}
		}
	}).catch(err => {
		res.send(err)
	})
}

//LOGIN CONTROLLER
module.exports.login = (req, res) => {

	let email = req.body.email;
	let password = req.body.password;

	User.findOne({email: email}).then(foundUser => {
		if(foundUser == null){
			res.send({
				message: "User Not Found!"
			})
		}else {
			const isPasswordCorrect = bcrypt.compareSync(password, foundUser.password);
			if(isPasswordCorrect){
				let token = auth.createAccessToken(foundUser)
					res.send({ accessToken: auth.createAccessToken(foundUser)})
				}else {
					res.send({
						message: "Incorrect Password!"
					})
				}
			} 
		}).catch(err =>{
			return err;
		})
	}

/*NON-ADMIN CONTROLLERS*/

//DETAILS CONTROLLER
module.exports.getUserDetails = (req, res) => {

	let userId = req.user.id

	User.findById(userId).then(nonAdminUser => {
		res.send(nonAdminUser)
	}).catch(err => {
		res.send(err);
	})
}

//UPDATE DETAILS CONTROLLER
module.exports.updateUser = (req, res) => {

	let userId = req.user.id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
		shippingAddress: req.body.shippingAddress
	}
	let options = {
		new: true
	}

	User.findByIdAndUpdate(userId, updates, options).then(updatedUserDetails => {
		res.send({UpdatedUser: updatedUserDetails});
	}).catch(err=> {
		return err;
	})
}


/*ADMIN CONTROLLERS*/

//DETAILS CONTROLLER
module.exports.adminProfile = (req, res) => {

	let userId = req.user.id

	User.find({_id: userId}).then(adminUser => {
		res.send({admin: adminUser})
	}).catch(err => {
		res.send(err);
	})
}

//SET USER AS ADMIN
module.exports.setUserToAdmin = (req, res) => {

	let userId = req.body.userId

	let updates = {
		isAdmin: req.body.isAdmin
	}
	let options = {
		new: true
	}

	User.findByIdAndUpdate(userId, updates, options).then(updatedUserToAdmin => {
		res.send({SetUserToAdmin: updatedUserToAdmin});
	}).catch(err=> {
		return err;
	})
}